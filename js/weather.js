function initAutocomplete() {
  const autocomplete = new google.maps.places.Autocomplete(
    document.getElementById('autocomplete'), {
      types: ['geocode']
    });
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    const place = autocomplete.getPlace();
    document.getElementById('table').innerHTML = '<tr><th>Date</th><th>Temperature (Celsium)</th><th>Weather condition</th></tr>'
    if (!place.geometry) {
      return;
    }
    const lat = place.geometry.location.lat()
    const lng = place.geometry.location.lng()
    const url = `http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lng}&units=metric&APPID=84d77ff44331f7f2e8fd5777a8732c61`
    //weatherSearch(url)
    helpweather(url)
  });

}//weather[i].map(a=>a.main)

function helpweather(url) {
  $.ajax({
    url: url,
    success: function(data) {
      const table = document.querySelector('table');
      data.list.forEach((element, i) => {
        table.innerHTML += `<tr><td>${element.dt_txt}</td><td>${element.main.temp}</td><td>${element.weather.map(a=>a.main)}</td> </tr>`;
      });
    }
  })
}
// async function weatherSearch(url) {
//   let response = await fetch(url)
//   if (response.ok) {
//     const json = await response.json();
//     const time = (json.list).map(a => a.dt_txt)
//     const temp = (json.list).map(a => a.main.temp)
//     const weather = ((json.list).map(a => a.weather))
//     const table = document.querySelector('table');
//     (json.list).forEach((element, i) => {
//       table.innerHTML += `<tr><td>${element.dt_txt}</td><td>${element.main.temp}</td><td>${element.weather.map(a=>a.main))}</td> </tr>`;
//     });
//   }
// }
